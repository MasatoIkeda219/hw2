    package pm.data;

import java.util.ArrayList;
import javafx.scene.shape.Shape;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    ArrayList<Shape> shape;
    String bg;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {

    }
    
    public ArrayList<Shape> getShapeArray()
    {
        return shape;
    }
    
    public String getBG(){
        return bg;
    }
    
    public void setShapeArray(ArrayList<Shape> shapes)
    {
        shape = shapes;
    }
    
    public void setBG(String background)
    {
        bg = background;
    }
}
