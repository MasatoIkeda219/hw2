package pm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
        DataManager dataManager = (DataManager)data;
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        
        ArrayList<Shape> shapes = dataManager.getShapeArray();
        String bgColor = dataManager.getBG();
        if(bgColor == null)
        {
            bgColor = "#f2f2f2";
        }
        fillArrayWithShape(shapes, arrayBuilder);
        JsonArray nodesArray = arrayBuilder.build();      
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("Shapes", nodesArray)
                .add("Background", bgColor)
                .build();
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
        
    }
      
    private JsonObject makeRectangleJsonObject(Rectangle rectangle)
    {
        JsonObject jso = Json.createObjectBuilder()
                .add("Shape", "Rectangle")
                .add("X", "" + rectangle.getX() + "")
                .add("Y", "" + rectangle.getY() + "")
                .add("Height", "" +rectangle.getHeight()+ "")
                .add("Width", "" + rectangle.getWidth() + "")
                .add("ColorFill", "#" + rectangle.getFill().toString().substring(2))
                .add("OutlineColor", "#" + rectangle.getStroke().toString().substring(2))
                .add("OutlineThickness", "" + rectangle.getStrokeWidth() + "")
                .build();
        return jso;
    }
    
   
    
    private JsonObject makeEllipseJsonObject(Ellipse ellipse)
    {
        JsonObject jso = Json.createObjectBuilder()
                .add("Shape", "Ellipse")
                .add("X", "" + ellipse.getCenterX() + "")
                .add("Y", "" + ellipse.getCenterY() + "")
                .add("Height", "" +ellipse.getRadiusY()+ "")
                .add("Width", "" + ellipse.getRadiusX() + "")
                .add("ColorFill", "#" + ellipse.getFill().toString().substring(2))
                .add("OutlineColor", "#" + ellipse.getStroke().toString().substring(2))
                .add("OutlineThickness", "" + ellipse.getStrokeWidth() + "")
                .build();
        return jso;
    }
    
    private void fillArrayWithShape(ArrayList<Shape> shape, JsonArrayBuilder arrayBuilder)
    {
        for(int i = 0; i < shape.size(); i++)
        {
            if(shape.get(i) instanceof Rectangle)
            {
                JsonObject rectObject = makeRectangleJsonObject((Rectangle)shape.get(i));
                arrayBuilder.add(rectObject);
            }
            else if(shape.get(i) instanceof Ellipse)
            {
                JsonObject ellipObject = makeEllipseJsonObject((Ellipse)shape.get(i));
                arrayBuilder.add(ellipObject);
            }
        }
    }
    
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
	dataManager.reset();
        
        JsonObject json = loadJSONFile(filePath);
        
        JsonArray jsonShapeArray = json.getJsonArray("Shapes");
        loadShapes(jsonShapeArray, dataManager);
        
        String bg = json.getString("Background");
        dataManager.setBG(bg);
        
        
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private void loadShapes(JsonArray jsonShapeArray, DataManager dataManager)
    {
        ArrayList<Shape> shapes = new ArrayList();
        for(int i = 0; i < jsonShapeArray.size(); i++)
        {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            Shape shape = loadShape(jsonShape);
            shapes.add(shape);
        }
        dataManager.setShapeArray(shapes);
    }
    
    private Shape loadShape(JsonObject jsonShape)
    {
        String type = jsonShape.getString("Shape");
        Shape shape = null;
        if(type.equals("Rectangle"))
        {
            shape = new Rectangle(Double.parseDouble(jsonShape.getString("X")),Double.parseDouble(jsonShape.getString("Y")),Double.parseDouble(jsonShape.getString("Width")),Double.parseDouble(jsonShape.getString("Height")));
            shape.setFill(Color.valueOf(jsonShape.getString("ColorFill").substring(0, 7)));
            shape.setStroke(Color.valueOf(jsonShape.getString("OutlineColor").substring(0, 7)));
            shape.setStrokeWidth(Double.parseDouble(jsonShape.getString("OutlineThickness")));
        }
        else if(type.equals("Ellipse"))
        {
           shape = new Ellipse(Double.parseDouble(jsonShape.getString("X")),Double.parseDouble(jsonShape.getString("Y")),Double.parseDouble(jsonShape.getString("Width")),Double.parseDouble(jsonShape.getString("Height")));
           shape.setFill(Color.valueOf(jsonShape.getString("ColorFill").substring(0, 7)));
            shape.setStroke(Color.valueOf(jsonShape.getString("OutlineColor").substring(0, 7)));
            shape.setStrokeWidth(Double.parseDouble(jsonShape.getString("OutlineThickness")));
        }
        
        return shape;
    }
    
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
