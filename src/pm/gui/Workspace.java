package pm.gui;

import java.awt.Canvas;
import java.awt.Point;
import javafx.scene.shape.Rectangle;
import java.io.IOException;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import javax.swing.filechooser.FileNameExtensionFilter;
import pm.data.DataManager;
import properties_manager.PropertiesManager;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    VBox leftPane;
    BorderPane rightPane;
    Pane canvas;

    HBox toolBar;
    HBox move;
    VBox bgColorPane;
    VBox fillColor;
    VBox outlineColor;
    VBox outlineThickness;
    HBox snapshotPane;
    
    Button selection = new Button();
    Button remove = new Button();
    Button rect = new Button();
    Button ellipse = new Button();
    Button back = new Button();
    Button front = new Button();
    

    Boolean selectionClicked = false;
    Boolean removedClicked = false;
    Boolean rectClicked = false;
    Boolean ellipseClicked = false;
    Boolean moveFClicked = false;
    Boolean moveBClicked = false;

    ArrayList<Shape> shapes = new ArrayList<Shape>();
    Point start;
    Rectangle rectangle;
    Ellipse ellipseD;
    Color temp;
    String background;
    
    
    //FileChooser save;
    //WritableImage snap;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        PropertiesManager propsSingleton = PropertiesManager.getPropertiesManager();

        DataManager dataManager = (DataManager) app.getDataComponent();
        dataManager.setShapeArray(shapes);
        
        
        
        workspace = new BorderPane();

        leftPane = new VBox();
        leftPane.getStyleClass().add("max_pane");
        canvas = new Pane();
        ColorPicker fc = new ColorPicker();
        ColorPicker outlineC = new ColorPicker();
        Slider outlineT = new Slider();

        toolBar = new HBox();
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + "SelectionTool.png";
        Image buttonImage = new Image(imagePath);
        selection.setGraphic(new ImageView(buttonImage));
        selection.setOnAction(e
                -> {
            rectClicked = false;
            selectionClicked = true;
            removedClicked = false;
            ellipseClicked = false;
            moveFClicked = false;
            moveBClicked = false;
            remove.setDisable(false);
            front.setDisable(false);
            back.setDisable(false);
            canvas.setCursor(Cursor.MOVE);
        });
       
        imagePath = FILE_PROTOCOL + PATH_IMAGES + "Remove.png";
        buttonImage = new Image(imagePath);
        remove.setGraphic(new ImageView(buttonImage));
        remove.setOnAction(e
                -> {
            rectClicked = false;
            selectionClicked = false;
            removedClicked = true;
            ellipseClicked = false;
            moveFClicked = false;
            moveBClicked = false;
            remove.setDisable(true);
            front.setDisable(true);
            back.setDisable(true);
            canvas.setCursor(Cursor.HAND);
            if (removedClicked == true) {
                if (rectangle != null) {
                    canvas.getChildren().remove(rectangle);
                } else if (ellipseD != null) {
                    canvas.getChildren().remove(ellipseD);
                }
            }
            gui.updateToolbarControls(false);        
        });
        
        
        imagePath = FILE_PROTOCOL + PATH_IMAGES + "Rect.png";
        buttonImage = new Image(imagePath);
        rect.setGraphic(new ImageView(buttonImage));
        rect.setOnAction(e
                -> {
            rectClicked = true;
            selectionClicked = false;
            removedClicked = false;
            ellipseClicked = false;
            moveFClicked = false;
            moveBClicked = false;
            remove.setDisable(true);
            front.setDisable(true);
            back.setDisable(true);
            canvas.setCursor(Cursor.CROSSHAIR);
            if (rectangle != null) {
                rectangle.setStroke(temp);
            }
            if (ellipseD != null) {
                ellipseD.setStroke(temp);
            }
        });
        
        imagePath = FILE_PROTOCOL + PATH_IMAGES + "Ellipse.png";
        buttonImage = new Image(imagePath);
        ellipse.setGraphic(new ImageView(buttonImage));
        ellipse.setOnAction(e
                -> {
            rectClicked = false;
            selectionClicked = false;
            removedClicked = false;
            ellipseClicked = true;
            moveFClicked = false;
            moveBClicked = false;
            remove.setDisable(true);
            front.setDisable(true);
            back.setDisable(true);
            canvas.setCursor(Cursor.CROSSHAIR);
            if (rectangle != null && rectangle.getFill().equals(Color.YELLOW)) {
                rectangle.setStroke(temp);
            }
            if (ellipseD != null && ellipseD.getFill().equals(Color.YELLOW)) {
                ellipseD.setStroke(temp);
            }
        });
        toolBar.getStyleClass().add("bordered_pane2");
        toolBar.getChildren().add(selection);
        toolBar.getChildren().add(remove);
        toolBar.getChildren().add(rect);
        toolBar.getChildren().add(ellipse);

        leftPane.getChildren().add(toolBar);
        
        
        move = new HBox();
        
        
        imagePath = FILE_PROTOCOL + PATH_IMAGES + "MoveToBack.png";
        buttonImage = new Image(imagePath);
        back.setGraphic(new ImageView(buttonImage));
        back.setPrefWidth(100);
        back.setOnAction(e
                -> {
            rectClicked = false;
            selectionClicked = false;
            removedClicked = false;
            ellipseClicked = false;
            moveFClicked = false;
            moveBClicked = true;
            if (rectangle != null) {
                rectangle.toBack();
                shapes.remove(rectangle);
                shapes.add(0,rectangle);
            }
            if (ellipseD != null) {
                ellipseD.toBack();
                shapes.remove(ellipseD);
                shapes.add(0,ellipseD);
            }
            canvas.setCursor(Cursor.HAND);
            gui.updateToolbarControls(false);
        
        });
        
        imagePath = FILE_PROTOCOL + PATH_IMAGES + "MoveToFront.png";
        buttonImage = new Image(imagePath);
        front.setGraphic(new ImageView(buttonImage));
        front.setPrefWidth(100);
        front.setOnAction(e
                -> {
            canvas.setCursor(Cursor.HAND);
            rectClicked = false;
            selectionClicked = false;
            removedClicked = false;
            ellipseClicked = false;
            moveFClicked = true;
            moveBClicked = false;
            if (rectangle != null) {
                rectangle.toFront();
                shapes.remove(rectangle);
                shapes.add(rectangle);
            }
            if (ellipseD != null) {
                ellipseD.toFront();
                shapes.remove(ellipseD);
                shapes.add(ellipseD);
            }
            gui.updateToolbarControls(false);
        
        });
        
        move.getStyleClass().add("bordered_pane2");
        move.getChildren().add(back);
        move.getChildren().add(front);

        leftPane.getChildren().add(move);

        bgColorPane = new VBox();
        Label bgColor = new Label("Background Color");
        bgColor.getStyleClass().add("labels");
        ColorPicker bg = new ColorPicker();
        bg.setOnAction(e -> {
            background = "#" + bg.getValue().toString().substring(2);
            dataManager.setBG(background);
            canvas.setStyle("-fx-background-color:" + background);
            gui.updateToolbarControls(false);
        
        });
        bgColorPane.getChildren().add(bgColor);
        bgColorPane.getChildren().add(bg);
        bgColorPane.getStyleClass().add("bordered_pane2");

        leftPane.getChildren().add(bgColorPane);

        fillColor = new VBox();
        Label fill = new Label("Fill Color");
        fill.getStyleClass().add("labels");
        fillColor.getChildren().add(fill);
        fillColor.getChildren().add(fc);
        fillColor.getStyleClass().add("bordered_pane2");

        leftPane.getChildren().add(fillColor);

        outlineColor = new VBox();
        Label outline = new Label("Outline Color");
        outline.getStyleClass().add("labels");
        outlineColor.getChildren().add(outline);
        outlineColor.getChildren().add(outlineC);
        outlineColor.getStyleClass().add("bordered_pane2");

        leftPane.getChildren().add(outlineColor);

        outlineThickness = new VBox();
        Label thickness = new Label("Outline Thickness");
        thickness.getStyleClass().add("labels");
        outlineThickness.getChildren().add(thickness);
        outlineThickness.getChildren().add(outlineT);
        outlineThickness.getStyleClass().add("bordered_pane2");

        leftPane.getChildren().add(outlineThickness);

        snapshotPane = new HBox();
        Button snapshot = new Button();
        imagePath = FILE_PROTOCOL + PATH_IMAGES + "Snapshot.png";
        buttonImage = new Image(imagePath);
        snapshot.setGraphic(new ImageView(buttonImage));
        snapshot.setPrefWidth(300);
        snapshot.setOnAction(e -> {
            FileChooser save = new FileChooser();
            save.setTitle("Save Snapshot As...");
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("PNG files *.png", "*.png");
            save.getExtensionFilters().add(filter);
            File snapFile = save.showSaveDialog(app.getGUI().getWindow());
            WritableImage snap = canvas.snapshot(new SnapshotParameters(),null);
            if (snapFile != null) {
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(snap, null), "png", snapFile);
            } catch (IOException h) {
                System.out.println("invalid");
            }
        }
        });




        snapshotPane.getChildren().add(snapshot);
        snapshotPane.getStyleClass().add("bordered_pane2");

        leftPane.getChildren().add(snapshotPane);

        ((BorderPane) workspace).setLeft(leftPane);
        ((BorderPane) workspace).setCenter(canvas);

        canvas.setOnMousePressed(f -> {
            if (rectClicked == true) {
                start = new Point(((int) f.getX()), ((int) f.getY()));
                rectangle = new Rectangle(start.x, start.y, (int) f.getX() - start.x, (int) f.getY() - start.y);
                rectangle.setFill(fc.getValue());
                rectangle.setStroke(outlineC.getValue());
                rectangle.setStrokeWidth(outlineT.getValue() + 1);
                canvas.getChildren().addAll(rectangle);
                shapes.add(rectangle);
                gui.updateToolbarControls(false);
        
            } else if (ellipseClicked == true) {
                start = new Point(((int) f.getX()), ((int) f.getY()));
                ellipseD = new Ellipse(start.x, start.y, f.getX() - start.x, f.getY() - start.y);
                ellipseD.setFill(fc.getValue());
                ellipseD.setStroke(outlineC.getValue());
                ellipseD.setStrokeWidth(outlineT.getValue() + 1);
                canvas.getChildren().addAll(ellipseD);
                shapes.add(ellipseD);
                gui.updateToolbarControls(false);
        
            }
        });
        canvas.setOnMouseDragged(f -> {
            if (rectClicked == true) {
                rectangle.setWidth(f.getX() - start.x);
                rectangle.setHeight(f.getY() - start.y);
            }
            if (ellipseClicked == true) {
                ellipseD.setRadiusX(f.getX() - start.x);
                ellipseD.setRadiusY(f.getY() - start.y);
            }

        });

        canvas.setOnMouseReleased(f -> {
            if (rectClicked == true) {
                rectangle = null;
            }
            if (ellipseClicked == true) {
                ellipseD = null;
            }
        });
        canvas.setOnMouseClicked(f -> {
            if (selectionClicked == true) {
                if (rectangle != null) {
                    rectangle.setStroke(temp);
                }
                if (ellipseD != null) {
                    ellipseD.setStroke(temp);
                }
                rectangle = null;
                ellipseD = null;
                if (f.getTarget() instanceof Rectangle) {
                    rectangle = (Rectangle) f.getTarget();
                    fc.setValue((Color) rectangle.getFill());
                    outlineC.setValue((Color) rectangle.getStroke());
                    outlineT.setValue(rectangle.getStrokeWidth());
                    temp = (Color) rectangle.getStroke();
                    rectangle.setStroke(Color.YELLOW);
                    rectangle.setOnMouseDragged(g -> {
                        rectangle.setX(g.getX());
                        rectangle.setY(g.getY());
                        gui.updateToolbarControls(false);
        
                    });

                } else if (f.getTarget() instanceof Ellipse) {
                    ellipseD = (Ellipse) f.getTarget();
                    fc.setValue((Color) ellipseD.getFill());
                    outlineC.setValue((Color) ellipseD.getStroke());
                    outlineT.setValue(ellipseD.getStrokeWidth());
                    temp = (Color) ellipseD.getStroke();
                    ellipseD.setStroke(Color.YELLOW);
                    ellipseD.setOnMouseDragged(g -> {
                        ellipseD.setCenterX(g.getX());
                        ellipseD.setCenterY(g.getY());
                        gui.updateToolbarControls(false);
        
                    });
                }
            }
        });
        fc.setOnAction(e -> {
            if (selectionClicked == true) {
                if (rectangle != null) {
                    rectangle.setFill(fc.getValue());
                }
                if (ellipseD != null) {
                    ellipseD.setFill(fc.getValue());
                }
                gui.updateToolbarControls(false);
        
            }
        });

        outlineC.setOnAction(e -> {
            if (selectionClicked == true) {
                if (rectangle != null) {
                    rectangle.setStroke(outlineC.getValue());
                    temp = outlineC.getValue();
                }
                if (ellipseD != null) {
                    ellipseD.setStroke(outlineC.getValue());
                    temp = outlineC.getValue();
                }
                gui.updateToolbarControls(false);
        
            }
        });

        outlineT.setOnMouseDragged(e -> {
            if (selectionClicked == true) {
                if (rectangle != null) {
                    rectangle.setStrokeWidth(outlineT.getValue());
                }
                if (ellipseD != null) {
                    ellipseD.setStrokeWidth(outlineT.getValue());
                }
                gui.updateToolbarControls(false);
        
            }
        });
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
        public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
        public void reloadWorkspace() {
            DataManager dataManager = (DataManager) app.getDataComponent();
            canvas.getChildren().clear();
            shapes = dataManager.getShapeArray();
            background = dataManager.getBG();
            canvas.setStyle("-fx-background-color:" + background);
            for(int i = 0; i < shapes.size(); i++)
            {
                canvas.getChildren().add(shapes.get(i));
            }
    }
}
